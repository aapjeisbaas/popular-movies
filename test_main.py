import unittest
import main
import json

class TestPopularMovies(unittest.TestCase):
    def setUp(self):
        # Initialize DynamoDB table before each test
        self.table = main.init_table()

    def create_event(self, params):
        return {"queryStringParameters": params}

    def test_popular_movies_basic(self):
        """Test basic popular movies fetch with default parameters"""
        event = self.create_event({"list": "popular", "max": "5"})
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        self.assertEqual(len(movies), 5)
        for movie in movies:
            self.assertIn("title", movie)
            self.assertIn("imdb_id", movie)
            self.assertIn("rating", movie)
            self.assertIn("year", movie)
            self.assertIn("votes", movie)
            self.assertIn("popular movies 100 rank", movie)

    def test_rating_filter(self):
        """Test filtering movies by rating"""
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "rating": "8.0"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        for movie in movies:
            self.assertGreaterEqual(movie["rating"], 8.0)

    def test_year_filter(self):
        """Test filtering movies by year"""
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "year": "2020"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        for movie in movies:
            self.assertGreaterEqual(movie["year"], 2020)

    def test_votes_filter(self):
        """Test filtering movies by minimum votes"""
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "votes": "100000"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        for movie in movies:
            self.assertGreaterEqual(movie["votes"], 100000)

    def test_fresh_filter(self):
        """Test filtering fresh movies (current year or next year)"""
        import datetime
        current_year = datetime.datetime.now().year
        
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "fresh": "true"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        # Ensure we got some movies
        self.assertGreater(len(movies), 0)
        
        # Check that at least one movie is from current year or next year
        found_fresh = False
        for movie in movies:
            if movie["year"] in [current_year, current_year + 1]:
                found_fresh = True
                break
        self.assertTrue(found_fresh, "No fresh movies (current or next year) found")

    def test_languages_filter(self):
        """Test filtering movies by language"""
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "languages": "English"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        self.assertGreater(len(movies), 0)

    def test_invalid_list(self):
        """Test handling of invalid list parameter"""
        event = self.create_event({
            "list": "invalid_list",
            "max": "5"
        })
        result = main.lambda_handler(event, None)
        self.assertIn("Wrong list selector", result)

    def test_combined_filters(self):
        """Test combining multiple filters"""
        event = self.create_event({
            "list": "popular",
            "max": "10",
            "rating": "7.0",
            "year": "2020",
            "votes": "50000"
        })
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        for movie in movies:
            self.assertGreaterEqual(movie["rating"], 7.0)
            self.assertGreaterEqual(movie["year"], 2020)
            self.assertGreaterEqual(movie["votes"], 50000)

    def test_missing_parameters(self):
        """Test with missing parameters (should use defaults)"""
        event = self.create_event({})
        result = main.lambda_handler(event, None)
        movies = json.loads(result)
        
        self.assertGreater(len(movies), 0)

if __name__ == '__main__':
    unittest.main()
