from imdb import IMDb
from functools import lru_cache
import json
import ast
import threading
import time
import math
import random
import boto3
from decimal import Decimal
import os
from flask import Flask, request, jsonify

# get the mem consumption down
import gc

gc.enable()

# used in fresh selection ;-)
import datetime

now = datetime.datetime.now()

# fancy printing of data
import pprint

pp = pprint.PrettyPrinter(indent=1)

threads = []
max_metadata_threads = 20

ia = IMDb()

movies_table = os.getenv("MOVIE_TABLE", default="Movies")
dynamodb_endpoint = os.getenv("DYNAMODB_ENDPOINT", default="http://localhost:8000")
region = os.getenv("AWS_REGION", default=None)

if region:
    dynamodb = boto3.resource("dynamodb", region_name=region)
else:
    dynamodb = boto3.resource("dynamodb", endpoint_url=dynamodb_endpoint, region_name="local")


def init_table():
    try:
        dynamodb.Table(movies_table).item_count
    except:
        dynamodb.create_table(
            TableName=movies_table,
            KeySchema=[{"AttributeName": "imdb_id", "KeyType": "HASH"}],
            AttributeDefinitions=[{"AttributeName": "imdb_id", "AttributeType": "S"}],
            BillingMode="PAY_PER_REQUEST",
        )
        created = False
        while not created:
            print("Waiting for table creation...")
            try:
                dynamodb.Table(movies_table).scan()
                created = True
            except:
                created = False
                time.sleep(1)
    return dynamodb.Table(movies_table)


table = init_table()


@lru_cache(maxsize=None)
def metadata(movie):
    try:
        record = table.get_item(Key={"imdb_id": movie.getID()})["Item"]
        return record
    except:
        print("Update metadata for: " + movie.data["title"])
        ia.update(movie, info=["main"])
        try:
            record = {
                "imdb_id": movie.getID(),
                "title": movie.data["title"],
                "rating": movie.data["rating"],
                "year": movie.data["year"],
                "votes": movie.data["votes"],
                "languages": movie.data["languages"],
                "genres": movie.data["genres"],
            }
            table.put_item(Item=json.loads(json.dumps(record), parse_float=Decimal))
            return record
        except:
            return movie.data


def threadedMovieLangCheck(movie, languages, result):
    try:
        if metadata(movie)["languages"][0] in languages:
            result.append(movie)
    except:
        print("No languages found for: " + movie.data["title"])


@lru_cache(maxsize=None)
def get_list(listName):
    print('getting list: ' + listName)
    if listName == "popular":
        return ia.get_popular100_movies()
    elif listName == "top":
        return ia.get_top250_movies()
    elif str(listName).startswith("ls"):
        try:
            return ia.get_movie_list(list_=listName)
        except:
            raise
    else:
        raise


def list_normalize(movieList):
    table = []
    try:
        movieList[0].data["top 250 rank"]
        rankingFieldName = "top 250 rank"
    except:
        pass

    try:
        movieList[0].data["popular movies 100 rank"]
        rankingFieldName = "popular movies 100 rank"
    except:
        pass

    try:
        movieList[0].data["rank"]
        rankingFieldName = "rank"
    except:
        pass

    try:
        for item in movieList:
            table.append(
                {
                    "title": item.data["title"],
                    "imdb_id": "tt" + item.getID(),
                    "rating": item.data["rating"],
                    "year": item.data["year"],
                    "votes": item.data["votes"],
                    rankingFieldName: item.data[rankingFieldName],
                }
            )
    except:
        table = movieList
    return table


def list_print(movieList):
    pp.pprint(list_normalize(movieList))


def list_json(movieList):
    return json.dumps(list_normalize(movieList))


def filter_list(
    movieList, rating=0, year=0, votes=0, max=None, fresh=False, languages=False
):
    result = []
    tempResult = []

    try:
        movieList = get_list(movieList)
    except:
        return [{"list not found": movieList}]

    if fresh:
        # For fresh movies, include current year and next year
        current_year = now.year
        for item in movieList:
            try:
                if (item.data["rating"] >= rating and 
                    item.data["year"] in [current_year, current_year + 1] and 
                    item.data["votes"] >= votes):
                    tempResult.append(item)
            except:
                # Skip movies with missing data
                pass
    else:
        for item in movieList:
            try:
                if (item.data["rating"] >= rating and 
                    item.data["year"] >= year and 
                    item.data["votes"] >= votes):
                    tempResult.append(item)
            except:
                # Skip movies with missing data
                pass

    if languages:
        for movie in tempResult[: max + math.ceil(max * 0.1)]:
            while threading.active_count() > max_metadata_threads:
                time.sleep(0.1)
            t = threading.Thread(
                target=threadedMovieLangCheck, args=[movie, languages, result]
            )
            t.daemon = True
            threads.append(t)
            t.start()
        while threading.active_count() > 1:
            time.sleep(0.1)
    else:
        result = tempResult

    return result[:max]


def lambda_handler(event, context):
    # print("Received event: " + json.dumps(event, indent=2))

    # queryStringParameters transform to function params
    try:
        movieList = event["queryStringParameters"]["list"]
    except:
        movieList = "popular"
    if movieList not in ["top", "popular"]:
        if not str(movieList).startswith("ls"):
            return json.dumps(
                "Wrong list selector '"
                + movieList
                + "' You can use: top, popular or ls#########"
            )

    try:
        reqRating = float(event["queryStringParameters"]["rating"])
    except:
        reqRating = 0

    try:
        reqYear = int(event["queryStringParameters"]["year"])
    except:
        reqYear = 0

    try:
        reqVotes = int(event["queryStringParameters"]["votes"])
    except:
        reqVotes = 0

    try:
        reqMax = int(event["queryStringParameters"]["max"])
    except:
        reqMax = 10000

    try:
        reqFresh = bool(ast.literal_eval(event["queryStringParameters"]["fresh"].lower()))
    except:
        reqFresh = False

    try:
        reqLanguages = event["queryStringParameters"]["languages"].split(",")
    except:
        reqLanguages = False

    # get the result with filters applied
    resultList = filter_list(
        movieList=movieList,
        rating=reqRating,
        year=reqYear,
        votes=reqVotes,
        max=reqMax,
        fresh=reqFresh,
        languages=reqLanguages,
    )

    # log requested query
    print(
        json.dumps(
            {
                "rating": reqRating,
                "year": reqYear,
                "votes": reqVotes,
                "max": reqMax,
                "fresh": reqFresh,
                "list": movieList,
            }
        )
    )

    gc.collect()
    # return json
    return list_json(resultList)


app = Flask(__name__)

@app.route('/movies')
def get_movies():
    rating = float(request.args.get('rating', 5))
    year = int(request.args.get('year', 2000))
    votes = int(request.args.get('votes', 10000))
    max_results = int(request.args.get('max', 100))
    fresh = request.args.get('fresh', '').lower() == 'true'
    movie_list = request.args.get('list', 'popular')
    languages = request.args.get('languages', '').split(',') if request.args.get('languages') else None

    filtered_movies = filter_list(
        movieList=movie_list,
        rating=rating,
        year=year,
        votes=votes,
        max=max_results,
        fresh=fresh,
        languages=languages
    )
    return list_json(filtered_movies)

if __name__ == "__main__":
    print("Initializing DynamoDB table...")
    table = init_table()
    print("Starting Flask application...")
    app.run(host='0.0.0.0', port=8080, debug=True)
