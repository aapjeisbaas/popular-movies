# popular-movies

![Hits](https://hitcounter.pythonanywhere.com/count/tag.svg?url=https%3A%2F%2Fgitlab.com%2Faapjeisbaas%2Fpopular-movies)

An api to pull in top popular movies from imdb

> This api was inspired by Steven Lu's [popular-movies](https://github.com/sjlu/popular-movies) project.

I wanted to follow and filter the imdb [moviemeter](https://www.imdb.com/chart/moviemeter/) list but couldn't find a direct api or a list from Steven Lu that reflected what I was looking for. So that's where this project comes in.

### [Blog post about this project](https://aapjeisbaas.nl/project/popular-movies/)

## options
| Query Param   | Effect                                       | value ranges                  |
| ------------- |:-------------------------------------------- | ----------------------------: |
| fresh         | Only return current and last year releases   | true / false                  |
| year          | Return $year and newer (disabled with fresh) | 4 number year                 |
| rating        | Return $rating and higher                    | 0 - 10                        |
| votes         | Return movies with at least this many votes  | 1 - ∞                         |  
| max           | Max returned movies                          | 1 - ∞                         |
| list          | imdb chart/list source default=popular       | popular / top / ls058726648   |
| languages     | List of accepted main languages in results   | Icelandic,Hindi,English,Dutch |

> The `languages` param is pretty heavy and increases response times of the api, if you're fine with the results without this filter please leave it out.

Example Queries:

- https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=6&votes=50000&list=popular
- https://aapjeisbaas.nl/api/v1/popular-movies/imdb?year=2010&votes=50000&rating=6&max=10&list=ls058726648
- https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&votes=10000&rating=7&max=10&list=ls016522954
- https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&list=top
- https://aapjeisbaas.nl/api/v1/popular-movies/imdb?list=ls027181777   < All Marvel Avengers Movies - Chronological MCU Timeline

## curl | jq

```
$ curl -s -X GET 'https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=3&rating=6&votes=50000&list=popular' |jq
[
  {
    "title": "Extraction",
    "imdb_id": "tt8936646",
    "rating": 6.8,
    "year": 2020,
    "votes": 101076,
    "popular movies 100 rank": 1
  },
  {
    "title": "Star Wars: Episode IX - The Rise of Skywalker",
    "imdb_id": "tt2527338",
    "rating": 6.7,
    "year": 2019,
    "votes": 319690,
    "popular movies 100 rank": 2
  },
  {
    "title": "Once Upon a Time... in Hollywood",
    "imdb_id": "tt7131622",
    "rating": 7.7,
    "year": 2019,
    "votes": 462778,
    "popular movies 100 rank": 6
  }
]

```
## Charts you can query

| list param    | IMDb link                                                 | api example   |
|:------------- |:--------------------------------------------------------- |:------------- |
| top           | https://www.imdb.com/chart/top/                           | https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&list=top |
| popular       | https://www.imdb.com/chart/moviemeter/                    | https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&list=popular |
| ls#########   | Any public [list](https://www.imdb.com/list/ls058726648/) | https://aapjeisbaas.nl/api/v1/popular-movies/imdb?year=2010&votes=50000&rating=6&max=10&list=ls058726648 |


## Radarr
This list by itself isn't all that exiting, the magic happens when you use it in Radarr as a list source. This api uses a structure that is compatible with the [StevenLuImport](https://github.com/Radarr/Radarr/wiki/Supported-NetImports#stevenluimport) so you can just input the query in the URL field.

> Please use at a sensible rate of max 1 query per hour in your radar setup.

There seems to be an issue with the auto library clean future and StevenLu lists, so if you use this list you need to set: **Clean Library Level** to: **Log only**
There are already multiple github issues open to solve this, unfortunatley this is not in the regular releases yet. (In the aphrodite branch this is fixed, but trust me it is unstable and you don't want to run it.)
[issue 4014](https://github.com/Radarr/Radarr/issues/4014)
[pull req](https://github.com/Radarr/Radarr/pull/4016)

## Most used queries
If more people use the same query you help improve the cache hit ratio and lower the server utilization.

| Most to least popular (top 20) |
| - |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=6&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&list=top |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=6&votes=50000&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=50&rating=6&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=30&rating=6&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?list=ls087717593 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?list=ls064081130 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=200&rating=6&votes=1000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=False&max=20&rating=6&votes=50000&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=100&rating=6&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=7&votes=50000&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=7&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=6.5&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?year=2018&max=500&rating=6&votes=20000&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?year=2018&rating=6&votes=50000&list=popular |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&votes=10000&rating=7&max=10&list=ls016522954 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=25&rating=7&votes=10000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=20&rating=3&votes=50000 |
| https://aapjeisbaas.nl/api/v1/popular-movies/imdb?fresh=True&max=50&rating=6&votes=10000 |

## Running with Docker

You can run this project locally using Docker and Docker Compose. This setup includes both the API service and a local DynamoDB instance.

### Prerequisites
- Docker
- Docker Compose

### Running the Application

1. Clone the repository:
```bash
git clone https://gitlab.com/aapjeisbaas/popular-movies.git
cd popular-movies
```

2. Build and start the containers:
```bash
docker-compose up --build
```

3. The API will be available at:
- http://localhost:8080/movies

### Example API Calls

Using curl:
```bash
# Get popular movies with rating >= 7
curl "http://localhost:8080/movies?list=popular&rating=7&max=5"

# Get fresh movies (current year)
curl "http://localhost:8080/movies?fresh=true&max=10"

# Get movies with specific filters
curl "http://localhost:8080/movies?list=popular&rating=7&votes=50000&max=5&fresh=true"
```



## Donate
If you like this api and want to give something back please donate to keep it around and up to date.

| method | where                                                           |
|:------ |:--------------------------------------------------------------- |
| XRP    | rPThf2QWMKKkmQM8hw9yD4bwpbmLYiz6vB                              |
| ETH    | 0xe589531E35fA84d9d561f028070663004aca5Fa4                      |
| BCH    | bitcoincash:qr5t82vekd7ekx7hsfkdqlwsws67nq3dcg6hn92u8x          |
| BTC    | 15aaSnoGBUMxyGwt7JveSNT75hBCL14wu1                              |
| iDeal  | https://bunq.me/svb/_/movies-api                                |

Icon made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fpopular-movies%2FREADME.md&dt=popular-movies)
